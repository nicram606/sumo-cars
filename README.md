# King of the hill
A project built with help of Renaissance Coders tutorial to introduce vehicle physics.

The game idea is simple - get the crown and hold it to the end of the round.
If you can do so, you win, otherwise you lose. There's always only one winner
of the round.

Battleroyale-like gameplay makes the game easy to learn and hard to master
> e.g. holding the crown for the whole round may be difficult - maybe it's
> easier to get it at the end of the round?

You can play agaist AI only or invite another human player for local multiplayer
session.

*Authors:* Marcin Wróblewski
