﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrownManager : MonoBehaviour
{
    public FollowingCrownController crownController;
    public GameObject currentWinner;
    private float lastSwitchTime;
    public float winnerSwitchCooldown = 1;

    public void DetermineWinner(GameObject player1, GameObject player2) {
        RoundManager roundManager = GameObject.Find("__RoundManager").GetComponent<RoundManager>();
        if (!roundManager.roundInProgress) {
            return;
        }

        if (!player1.name.Equals(currentWinner.name) && !player2.name.Equals(currentWinner.name)) {
            return;
        }
        
        if (lastSwitchTime + winnerSwitchCooldown < Time.fixedTime) {
            lastSwitchTime = Time.fixedTime;
            var currentWinner = player1.name.Equals(this.currentWinner.name) ? player2 : player1;
            SetCurrentWinner(currentWinner);
        }
    }

    private void SetCurrentWinner(GameObject winner) {
        currentWinner = winner;
        crownController.SetObjectToFollow(currentWinner.transform);
    }

    void Start() {
        lastSwitchTime = Time.fixedTime;
        var players = GameObject.FindGameObjectsWithTag("Player");
        SetCurrentWinner(players[Random.Range(0, players.Length - 1)]);
    }
}
