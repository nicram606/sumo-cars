﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinsManager : MonoBehaviour
{
    public void SaveSkinIndex(int index) {
        PlayerPrefs.SetInt("skin", index);
    }
}
