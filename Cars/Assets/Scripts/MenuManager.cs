﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public string aiPlaySceneName = "PlayAI";
    public string localPlaySceneName = "PlayLocal";

    public void LoadPlayAiLevel() {
        SceneManager.LoadScene(aiPlaySceneName);
    }

    public void LoadPlayLocalLevel() {
        SceneManager.LoadScene(localPlaySceneName);
    }

    public void Quit() {
        Application.Quit();
    }
}
