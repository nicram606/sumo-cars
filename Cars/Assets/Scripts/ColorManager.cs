﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorManager : MonoBehaviour
{
    public List<Material> materials;
    public List<Renderer> parts;

    void Start() {
        int materialIndex = PlayerPrefs.GetInt("skin", 0);
        SetMaterial(materials[materialIndex]);
    }

    public void SetMaterialByIndex(int index) {
        SetMaterial(materials[index % materials.ToArray().Length]);
    }

    private void SetMaterial(Material material) {
        foreach (Renderer part in parts)
        {
            part.material = material;
        }
    }
}
