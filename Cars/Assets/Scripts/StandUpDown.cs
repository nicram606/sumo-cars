﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandUpDown : MonoBehaviour
{
    public float maxY = 0, minY = -10;
    public bool isUp = true, randomize = false;
    public float maxSpeed = 0.03f;
    private float nextChangeTime = 0;
    void Start()
    {
        transform.position = new Vector3(transform.position.x, isUp ? maxY : minY, transform.position.z);
    }

    void FixedUpdate()
    {
        if (isUp && transform.position.y <= maxY) {
            transform.position += new Vector3(0, maxSpeed, 0);
        } else if (!isUp && transform.position.y >= minY) {
            transform.position += new Vector3(0, -maxSpeed, 0);
        } else {
            bool shouldChange = (Time.fixedTime > nextChangeTime) && randomize;
            if (shouldChange) {
                isUp = !isUp;
                if (randomize) {
                    nextChangeTime = Time.fixedTime + Random.Range(2, 10);
                }
            }
        }
    }
}
