﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RoundManager : MonoBehaviour
{
    public float roundPeriodInSeconds = 120;
    private float timeLeft = 0;
    private float lastTimerUpdateTime = 0;
    public Text counter;
    public bool roundInProgress = false;
    public GameObject roundEndUI;
    public Text roundWinnerText, roundWinnerDescText;
    private Dictionary<string, int> points = new Dictionary<string, int>();

    public MusicManager musicManager;

    void Start()
    {
        SetupPoints();
        SetupRound();
    }

    private void SetupRound() {
        roundEndUI.SetActive(false);
        timeLeft = roundPeriodInSeconds;
        Tick();
    }

    private void SetupPoints() {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (var player in players)
        {
            points.Add(player.name, 0);
        }
    }

    public void Restart() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void BackToMenu() {
        SceneManager.LoadScene("MainMenu");
    }

    void FixedUpdate()
    {
        if (roundInProgress && Mathf.FloorToInt(Time.fixedTime) != Mathf.FloorToInt(lastTimerUpdateTime)) {
            Tick();
            if (timeLeft == 0) {
                EndGame();
            }
        }
    }

    void Tick() {
        lastTimerUpdateTime = Time.fixedTime;
        timeLeft--;
        counter.text = FormatTimer(timeLeft);
        roundInProgress = timeLeft >= 0;
        CrownManager crownManager = GameObject.Find("__CrownManager").GetComponent<CrownManager>();
        if (crownManager.currentWinner == null) {
            return;
        }
        points[crownManager.currentWinner.name] += 1;
    }

    private string FormatTimer(float secondsLeft) {
        int minutes = Mathf.FloorToInt(secondsLeft / 60);
        int seconds = Mathf.FloorToInt(secondsLeft % 60);
        return minutes + ":" + (seconds < 10 ? "0": "") + seconds;
    }

    private void EndGame() {
        CrownManager crownManager = GameObject.Find("__CrownManager").GetComponent<CrownManager>();
        counter.enabled = false;
        roundWinnerText.text = crownManager.currentWinner.name + " won";
        roundWinnerDescText.text = "Hold crown for " + points[crownManager.currentWinner.name] + (points[crownManager.currentWinner.name] == 1 ? " second" : " seconds");
        roundEndUI.SetActive(true);
        musicManager.StartWinMusic();
    }
}
