﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleCarController : MonoBehaviour {

	public bool rearWheelsAcceleration = false;
	public bool rearWheelsSteering = false;
	public string playerInputSuffix = "";
	public AudioSource carSound;
	public void GetInput()
	{
		m_horizontalInput = Input.GetAxis("Horizontal" + playerInputSuffix);
		m_verticalInput = Input.GetAxis("Vertical" + playerInputSuffix);
	}

	private void Steer()
	{
		m_steeringAngle = maxSteerAngle * m_horizontalInput;
		frontDriverW.steerAngle = m_steeringAngle;
		frontPassengerW.steerAngle = m_steeringAngle;
		if (rearWheelsSteering) {
			rearDriverW.steerAngle = - m_steeringAngle / 8;
			rearPassengerW.steerAngle = - m_steeringAngle / 8;
		}
	}

	private void Accelerate()
	{
		frontDriverW.motorTorque = m_verticalInput * motorForce;
		frontPassengerW.motorTorque = m_verticalInput * motorForce;
		if (rearWheelsAcceleration) {
			rearDriverW.motorTorque = m_verticalInput * motorForce / 8;
			rearPassengerW.motorTorque = m_verticalInput * motorForce / 8;
		}
	}

	private void UpdateWheelPoses()
	{
		UpdateWheelPose(frontDriverW, frontDriverT);
		UpdateWheelPose(frontPassengerW, frontPassengerT);
		UpdateWheelPose(rearDriverW, rearDriverT);
		UpdateWheelPose(rearPassengerW, rearPassengerT);
	}

	private void UpdateWheelPose(WheelCollider _collider, Transform _transform)
	{
		Vector3 _pos = _transform.position;
		Quaternion _quat = _transform.rotation;

		_collider.GetWorldPose(out _pos, out _quat);

		_transform.position = _pos;
		_transform.rotation = _quat;
	}

	private void HoldCarStraight() {
		if (IsAnyWheelInAir())
		{
			Quaternion fixedRotation = Quaternion.Lerp(transform.localRotation, Quaternion.identity, 15 * Time.deltaTime);
			transform.localRotation = new Quaternion(fixedRotation.x, transform.localRotation.y, fixedRotation.z, transform.localRotation.w);
		}
	}

	private bool IsAnyWheelInAir() {
		WheelHit hit = new WheelHit();
		return !rearDriverW.GetGroundHit(out hit) &&
		 !rearPassengerW.GetGroundHit(out hit) &&
		 !frontPassengerW.GetGroundHit(out hit) &&
		 !frontDriverW.GetGroundHit(out hit);
	}

	private void AdjustPitch() {
		carSound.pitch = 2 + Mathf.Abs(m_verticalInput);
	}
	private void FixedUpdate()
	{
		GetInput();
		Steer();
		Accelerate();
		UpdateWheelPoses();
		HoldCarStraight();
		AdjustPitch();
	}

	private float m_horizontalInput;
	private float m_verticalInput;
	private float m_steeringAngle;

	public WheelCollider frontDriverW, frontPassengerW;
	public WheelCollider rearDriverW, rearPassengerW;
	public Transform frontDriverT, frontPassengerT;
	public Transform rearDriverT, rearPassengerT;
	public float maxSteerAngle = 30;
	public float motorForce = 50;
}
