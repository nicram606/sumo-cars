﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public AudioSource music, winMusic;

    void Start() {
        music.Play();
        winMusic.Stop();
    }

    public void StartWinMusic() {
        music.Stop();
        winMusic.Play();
    }
}
