﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrownManagerDelegate : MonoBehaviour
{
    private CrownManager crownManager;
    
    void Start() {
        crownManager = GameObject.FindGameObjectWithTag("CrownManager").GetComponent<CrownManager>();
    }

    void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag.Equals("Player")) {
            crownManager.DetermineWinner(gameObject, collision.gameObject);
        }
    }
}
